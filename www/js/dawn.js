$(document).ready(() => {
  let sourceUrl = 'http://127.0.0.1:8000/api/'//'https://dad.petrongsoftware.com/api/';

  function alertFunction(message){
    //navigator.notification.
    alert(message)
  }

  function continueToDashView() {
    $(".common").hide();
    $(".startPage").hide();
    $(".continuePage").show();
    $(".ConvertsView").hide();
    $(".soulWinnerView").hide();
  }
  const loaderHide = () => {
    $("#loader").hide();
    $(".loadCover").hide();
  }
  const loaderShow = () => {
    $("#loader").show();
    $(".loadCover").show();
  }
  const isConvert = () => {
    userProfile = JSON.parse(localStorage.getItem("LoginResult"))
    if (userProfile.user.user == 'convert') return true
    else return false
  }
  function dashboardView() {
    $(".common").hide();
    $(".pages").hide();
    if (isConvert()) {
      $(".cvFooterDown").hide()
      $(".cvDashboard").show();
    } else {
      $(".swvFooterDown").hide()
      $(".swvDashboard").show();
    }
  }
  function profileView() {
    $(".pages").hide();
    if (isConvert()) {
      $(".cvFooterDown").hide()
      $(".cvProfile").show()
    }
    else {
      $(".swvFooterDown").hide()
      $(".swvProfile").show()
    }
  }
  function ChatView(ChatTypeId, ConvertInChatId) {
    $(".pages").hide();
    $(".chatting").show();
  } //Has to show what the chat is about
  const growthAnalytics = (viewsType, convertsId) => {
    loaderShow()
    $.ajax({
      type: "GET",
      url: `${sourceUrl}growth/index/${convertsId}`,
      crossDomain: true,
      headers: { "Content-Type": "application/json" },
      error: () => {},
      success: (result) => {
        $('#studiesTable').text('')
        viewsType ? $('.convertsDetail').show() : $('.convertsDetail').hide()
        result.scoreTable.map(item => {
          let tr = `
          <div class="row growthT" key="${item[0]}">
            <div class="col-xs-1"> <span class="fa fa-circle" style="color: purple"></span> </div>
            <div class="col-xs-8">
              <h5 style="text-align: left">${item[0]}</h5>
            </div>
            <div class="col-xs-3">
              <h5 style="text-align: right">${item[2]}/${item[1]}</h5>
            </div>
          </div>`
          $('#studiesTable').append(tr)
        })
        result.scoreTable.map(item => {
          let tr = `
          <div class="row growthT" key="${item[0]}">
            <div class="col-xs-1"> <span class="fa fa-circle" style="color: purple"></span> </div>
            <div class="col-xs-8" style='display: flex; align-content: center; min-height: 25px'>
              <h5 style="text-align: left">${item[0]}</h5>
            </div>
            <div class="col-xs-2">
              <h5 style="text-align: right">${item[2]}/${item[1]}</h5>
            </div>
          </div>`
          $('#activitiesTable').append(tr)
        })
        
        
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
          let  datas=[]
          result.activityChart.map(i=>{
            return( datas.push("['"+i[0]+"',"+i[1]+"]") )
          })
          var data = google.visualization.arrayToDataTable([
            datas
          ]);

          var options = {
            // title: 'Company Performance',
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          var chart = new google.visualization.LineChart(document.getElementById('activityChart'));

          chart.draw(data, options);
        }
            
        $(".pages").hide();
        $(".growthAnalytics").show();
        loaderHide()
      }
    })
  }
  
  function chooseCounselorView(choosenSectionId) {
    $("#counselSection").text(choosenSectionId);
    $(".pages").hide();
    $(".chooseCounselor").show();
    loaderShow()
    counselorArray = [];
    $(".counsellorList").text('');
    counselorArray.forEach(counselors => {
      let counselorsRow = `
      <div class="coucCard chooseCouncellor" id="${counselors.soulwinner_id}" key="${counselors.soulwinner_id}">
        <div class="roundedImg"> <img src="img/assets/cal-l.jpg" /> </div>
        <div class="coucText">
          <h4>${counselors.name}</h4>
          <p>${counselors.status}</p>
        </div>
      </div>`;
      $(".counsellorList").append(counselorsRow);
    });
    loaderHide()
    let chooseCouncellor = document.querySelectorAll(".chooseCouncellor");
    for (let d = 0; d < chooseCouncellor.length; d++) {
      chooseCouncellor[d].addEventListener(
        "click", function () {
          ChatView(this.id)
        }, true
      );
    }
  }
  function quizPage(topic_id, total, quizzes, series_id, series_title) {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    let i = -1;
    $(".pages").hide();
    $(".quizQue").show();
    let answer = new Array(total) // Preallocation for performance
    next()
    function next() {
      if (i != total) {
        let current = answer[i]
        if (!current && i>=0 ) alertFunction("Please select an answer to proceed.")
        else i = i + 1
        if (i < total) {
          quiz = quizzes[i]
          let val = answer[i]
          if (val) $("input[name=optradio][value="+ val +"]").prop('checked', true);
          else $("input[name=optradio]").prop('checked', false);
          $("#theQuestionNumb").text(`Question: ${i + 1} of ${total}`);
          $("#theQuestion").text(quiz.question);
          $("#optionA").text(quiz.optiona);
          $("#optionB").text(quiz.optionb);
          $("#optionC").text(quiz.optionc);
          $("#optionD").text(quiz.optiond);
          stage(i)
        }
      }
    }

    function back() {
      if (i > 0) {
        if (i == total) i = i - 2
        else i = i - 1
        if (i < total) {
          quiz = quizzes[i]
          let val = answer[i]
          console.log('val: ', val);
          if (val) $("input[name=optradio][value="+ val +"]").prop('checked', true);
          else $("input[name=optradio]").prop('checked', false);
          $("#theQuestionNumb").text(`Question: ${i + 1} of ${total}`);
          $("#theQuestion").text(quiz.question);
          $("#optionA").text(quiz.optiona);
          $("#optionB").text(quiz.optionb);
          $("#optionC").text(quiz.optionc);
          $("#optionD").text(quiz.optiond);
          stage(i)
        }
      }
    }

    function stage(j) {
      $(".quizStage").text("");
      let quizMoves = `<div class="container-fluid text-center">
        <div class="row" style="padding: 15px">
          <div class="col-xs-6 backBtnQuiz" style="color: white">
            <p> <span class="prevBtn fa fa-chevron-left backwardd" style="margin-right: 8px"></span> Back </p>
          </div>
          <div class="col-xs-6 continueBtnQuiz" style="color: white">
            <p> Next <span class="prevBtn fa fa-chevron-right forwardd" style="margin-left: 8px"></span> </p>
          </div>
        </div>
      </div>`
      let quizStart = `<div class="container-fluid text-center">
        <div class="row">
          <div class="col-xs-12" style="padding: 30px">
            <div class="takeQuizBtnQuiz">
              <p> <span class="zmdi zmdi-star" style="padding: 10px"></span> Submit </p>
            </div>
          </div>
        </div>
      </div>`
      if (j == total - 1) $(".quizStage").append(quizStart);
      else $(".quizStage").append(quizMoves)

      $(".continueBtnQuiz").click(() => { next() })
      $(".backBtnQuiz").click(() => { back() })
      
      let eachTopic = document.querySelectorAll(".quizOpt");
      for (let d = 0; d < eachTopic.length; d++) {
        eachTopic[d].addEventListener(
          "click", function () {
            answer[i] = $(this).attr('value')
          }, false
        );
      }

      $(".takeQuizBtnQuiz").click(() => {
        let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
        if (!answer[j]) alertFunction("Please select an answer to proceed.")
        else {
          loaderShow()
          $.ajax({
            url: sourceUrl + "scores",
            type: "POST",
            crossDomain: true,
            headers: { Authorization: "Bearer " + userProfile.token },
            data: {
              user_id : userProfile.user.id,
              topic_id : topic_id,
              answers : answer,
            },
            dataType: "json",
            error: () => { loaderHide() },
            success: (result) => {
              if (result.status == true) {
                $('#scoredPercent').text(result.percent + "%")
                $('#scored').text(result.score)
                $('#scoreTotal').text(" / " + result.total_quiz)
                $("#modalScore").modal("show");
                loaderHide()
                
                $(".scoreClose").click(() => {
                  $("#modalScore").modal("hide");
                  topicsView(series_id, series_title)
                })
              }
            },
          })
        }
      })
    }
  }
  function studyPage(title, topic_id, series_id, series_title) {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    let token = userProfile.token;
    let total = 0
    let quizTotal = 0
    let i = -1;
    let studies = [];
    let quiz = [];
    $(".topicName").text(title); // Set Current Topic's title
    loaderShow()
    $.ajax({
      url: sourceUrl + `get_studies/with_topic_id/${topic_id}`,
      type: "GET",
      crossDomain: true,
      headers: { Authorization: "Bearer " + token },
      dataType: "json",
      error: () => { loaderHide() },
      success: (result) => {
        if (result.status == true) {
          total = result.total_studies
          quizTotal = result.total_quizzes //Total Number of quizzes
          $("#quizNo").text(quizTotal)
          studies = result.studies // Get all stidies
          quiz = result.quizzes // Get all quizzes
          next()
          loaderHide()
        }
      }
    });
    $(".pages").hide();
    $(".studysPage").show();

    function next() {
      if (i != total) {
        i = i + 1
        if (i < total) {
          study = studies[i]
          $(".studyImg").css("background-image", `url('http://127.0.0.1:8000${study.studyImage}')`);
          $(".convStdId").text(`${title} : ${i + 1}/${total}`);
          let scriptures = ""
          study.scriptures.forEach(element => {
            scriptures = scriptures + element + "; "
          });
          $(".convStdText").text(scriptures)
          $(".convStdTopic").text(study.title);
          $(".studyContent").text(study.content);
          $(".studyPonder").text(study.ponder);
          stage(i)
        }
      }
    }

    function back() {
      if (i > 0) {
        if (i == total) i = i - 2
        else i = i - 1
        if (i < total) {
          study = studies[i]
          $(".convStdId").text(`${title} : ${i + 1}/${total}`);
          $(".convStdTopic").text(study.title);
          $(".studyContent").text(study.content);
          $(".studyPonder").text(study.ponder);
          stage(i)
        }
      }
    }

    function stage(j) {
      $(".stage").text("");
      let moves = `<div class="container-fluid text-center">
        <div class="row" style="padding: 15px">
          <div class="col-xs-6 backBtn" style="color: white">
            <p> <span class="prevBtn fa fa-chevron-left backwardd" style="margin-right: 8px"></span> Back </p>
          </div>
          <div class="col-xs-6 continueBtn" style="color: white">
            <p> Next <span class="prevBtn fa fa-chevron-right forwardd" style="margin-left: 8px"></span> </p>
          </div>
        </div>
      </div>`
      let toQuiz = `<div class="container-fluid text-center">
        <div class="row">
          <div class="col-xs-12" style="padding: 30px">
            <div class="takeQuizBtn">
              <p> <span class="zmdi zmdi-star" style="padding: 10px"></span>Take Quiz </p>
            </div>
          </div>
        </div>
      </div>`
      if (j == total - 1) $(".stage").append(toQuiz);
      else $(".stage").append(moves)

      $(".continueBtn").click(() => { next() })
      $(".backBtn").click(() => { back() })
      $(".takeQuizBtn").click(() => {
        //Ajax call to get the quiz
        $(".pages").hide();
        $(".quiz").show()
        $(".quizStartBtn").click(() => {
          //Parameters : title of current topic's id, its quiz and total and Current Series and it's id
          quizPage(topic_id, quizTotal, quiz, series_id, series_title)
        })
      })
    }
  }
  function topicsView(series_id, title) {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    let token = userProfile.token;
    loaderShow()
    $.ajax({
      url: sourceUrl + `get_topics/with_category_id/${series_id}`,
      type: "GET",
      crossDomain: true,
      headers: { Authorization: "Bearer " + token },
      dataType: "json",
      error: () => { loaderHide() },
      success: (result) => {
        if (result.status == true) {
          $(".topicsRow").text("");
          $(".stdytitle").text(`${title}`);
          result.topics.forEach((topic, i) => {
            let eachTopic = `
            <div class="row taskCard bDBtn clickbtn" id="${topic.id}" name="${topic.name}" desc="${topic.description}">
              <div class="col-xs-3">
                <p>
                  <span
                    class="zmdi zmdi-calendar-alt"
                    style="padding-right: 5px" ></span>${i + 1}
                </p>
              </div>
              <div class="col-xs-9"> <p>${topic.name}</p> </div>
            </div>`;
            $(".topicsRow").append(eachTopic);
          });
            loaderHide()
            $(".pages").hide();
            $(".topics").show();
          let eachTopic = document.querySelectorAll(".clickbtn");
          for (let d = 0; d < eachTopic.length; d++) {
            eachTopic[d].addEventListener(
              "click", function () {
                $("#quizNote").text($(this).attr('desc'))
                //Parameters: the topic title, and id, and the currets Series id and title
                studyPage($(this).attr('name'), this.id, series_id, title);
              }, false
            );
          }
        }
      },
    });
  }

  const viewType = () => { //Decide which view to show
    if (isConvert()) {
      $(".ConvertsView").show();
      $(".soulWinnerView").hide();
    } else {
      $(".ConvertsView").hide();
      $(".soulWinnerView").show();
    }
  }
  const loadUserData = () => {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    let userdata = userProfile.user;
    if (userdata.user == "convert") {
      $(".convName").text(userdata.name);
      if (userdata.growth_index != null) $(".profilePageGrowthIndex").text(userdata.growth_index + '%');
      else $(".profilePageGrowthIndex").text('1%');
    } else if (userdata.user == "soul winner") {
      $(".soulWname").text(userdata.name);
    }
    if (userdata.profile_photo != null) {
      $(".profilePageImg").attr(
        "src", "data:image/jpeg;base64," + userdata.profile_photo
      );
    }
    if (userdata.name != null) $(".profilePageName").text(userdata.name);
    if (userdata.state != null) $(".profilePageState").text(userdata.state);
    if (userdata.gender == 'm') $(".profilePageGender").text('Male');
    if (userdata.gender == 'f') $(".profilePageGender").text('Female');
    if (userdata.biography != null) $(".profilePageBio").text(userdata.biography);
    if (userdata.phone != null) $(".profilePagePhone").text(userdata.phone);
    if (userdata.email != null) $(".profilePageEmail").text(userdata.email);
    if (userdata.region != null) $(".profilePageRegion").text(userdata.region);
    if (userdata.country != null) $(".profilePageCountry").text(userdata.country);
  }
  getStarted() //App starts here
  function getStarted() {
    $(".common").hide(); //Is user already logged in?
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    if (!userProfile) { //No User is Logged In
      $(".startPage").show();
      $(".ConvertsView").hide();
      $(".soulWinnerView").hide();
    }
    else { //User is Logged in
      if (userProfile.status) continueToDashView();
      loadUserData(); //Load the user data
    }
  }
  //Start Sign In / up
  $(".signUpBtn").click(() => {
    if ($(".full_nameReg").val() === "" || $(".emailReg").val() === "" || $(".passwordReg").val() === "") {
      $(".regErr").text("Please fill in all details");
      $(".regErr").show();
    }
    else {
      $(".regErr").hide();
      loaderShow()
      $.ajax({
        type: "POST",
        url: sourceUrl + "register",
        data: JSON.stringify({
          name: $(".full_nameReg").val(),
          email: $(".emailReg").val(),
          password: $(".passwordReg").val(),
          user: $("input[type='radio'][name='regRadio']:checked").val(),
          soulwinner_id: $(".soulWinnerId").val()
        }),
        dataType: "json",
        crossDomain: true,
        headers: { "Content-Type": "application/json" },
        error: (error) => {
          $(".regErr").text(...error);
          $(".regErr").show();
          loaderHide()
        },
        success: (result) => {
          loaderHide()
          if (result.status == false) {
            $(".regErr").text(...result.message);
            $(".regErr").show();
          }
          else if (result.status == true) {
            localStorage.setItem("LoginResult", JSON.stringify(result));
            $("#modalRegSucc").modal("show");
          }
        },
      });
    }
  });
  $(".closeRegModal").click(() => {
    $("#modalRegSucc").modal("hide");
    loadUserData(); //Load the user data
    continueToDashView();
  });
  $(".closeConfirmationModal").click(() => {
    $("#modalConfirmation").modal("hide");
  });
  $(".signInBtn").click(() => {
    if ($(".signInName").val() === "" || $(".signInPassword").val() === "") {
      $(".loginErr").text("Please enter your username and password");
      $(".loginErr").show();
    } else {
      loaderShow()
      $.ajax({
        type: "POST",
        url: sourceUrl + "login",
        data: JSON.stringify({
          email: $(".signInName").val(),
          password: $(".signInPassword").val(),
        }),
        dataType: "json",
        crossDomain: true,
        headers: { "Content-Type": "application/json" },
        error: () => {
          $(".loginErr").text("could not connect, please check your internet connection");
          $(".loginErr").show();
          loaderHide()
        },
        success: (result) => {
          if (result.status == false) {
            $(".loginErr").text(result.message);
            $(".loginErr").show();
            loaderHide()
          } else if (result.status == true) {
            localStorage.setItem("LoginResult", JSON.stringify(result));
            loadUserData()
            dashboardView()
            viewType()
            loaderHide()
          }
        },
      });
    }
  });

  const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  let today = new Date();
  let day = `${days[today.getDay()]} ${today.getDate()}, ${months[today.getMonth()]} ${today.getFullYear()}`;
  $("#day").text(day);
  //
  //Clicks
  $(".continueToDashBoard").click(() => {
    viewType() //You are logged in already
    dashboardView();
  });
  $(".getStartedBtn").click(() => {
    $(".startPage").hide();
    $(".signUpInPage").show();
  });
  $(".logout").click(() => {
    localStorage.removeItem("LoginResult");
    getStarted();
  });
  $(".radioDiv").click(() => { //To Add Referrer soul winner id
    let user = $("input[type='radio'][name='regRadio']:checked").val();
    if (user == "soul winner") {
      $(".soulWinnerId").hide();
    } else {
      $(".soulWinnerId").show();
    }
  });
  $(".profileBtn").click(() => {
    profileView();
  });
  $(".footProfile").click(() => {
    profileView();
  });
  $(".profileEdit").click(() => {
    $(".pages").hide();
    $(".editProfile").show();
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    $(".profileEditName").val(userProfile.user.name);
    $(".profileEditEmail").val(userProfile.user.email);
    $(".profileEditPhone").val(userProfile.user.phone);
    if (userProfile.user.gender == 'm') $(".profileEditGender").val('m');
    if (userProfile.user.gender == 'f') $(".profileEditGender").val('f');
    $(".profileEditBio").val(userProfile.user.biography);
    $(".profileEditState").val(userProfile.user.state);
    $(".profileEditCountry").val(userProfile.user.country);
    $(".profileEditRegion").val(userProfile.user.region);
  });
  $(".profileContd").click(() => {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));

    // let editprofileImage = document.querySelector(".profileImage");
    // let editprofileShow = document.querySelector(".editImageShow");

    // editprofileImage.onchange = function (e) {
    //   const file = e.target.files[0];
    //   encode the file using the FileReader API
    //   const reader = new FileReader();
    //   reader.onloadend = () => {
    //     use a regex to remove data url part
    //     const base64String = reader.result
    //       .replace("data:", "")
    //       .replace(/^.+,/, "");
    //     console.log(base64String);
    //     editprofileShow.src = "data:image/jpeg;base64," + base64String;
    //     updateForm.profile_photo = base64String;
    //   };
    //   reader.readAsDataURL(file);
    // };
    
    loaderShow()
    $.ajax({
      url: sourceUrl + "updateProfile/" + userProfile.user.id,
      type: "POST",
      crossDomain: true,
      headers: { Authorization: "Bearer " + userProfile.token },
      data: {
        name: $(".profileEditName").val(),
        email: $(".profileEditEmail").val(),
        user: userProfile.user.user,
        phone: $(".profileEditPhone").val(),
        gender: $(".profileEditGender").val(),
        biography: $(".profileEditBio").val(),
        state: $(".profileEditState").val(),
        country: $(".profileEditCountry").val(),
        region: $(".profileEditRegion").val(),
        password: $(".profileEditPassword").val(),
        profile_photo: "",
      },
      dataType: "json",
      error: () => { loaderHide() },
      success: (result) => {
        if (result.status == true) localStorage.setItem("LoginResult", JSON.stringify(result));
        loadUserData()
        loaderHide()
        profileView();
      },
    });
  });
  $(".backToDash").click(() => {
    dashboardView();
  });
  $(".swMyConverts").click(() => {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    $(".listOfConverts").text("")
    $(".pages").hide();
    $(".chooseConv").show();
    //$(".swvFooterDown").show()
    loaderShow()
    userProfile.relationship.forEach((myConverts, i) => {
      let growths = Math.floor((myConverts.activityGrowth + myConverts.scoreGrowth) / 2)
      let myConvertsRow = `
        <div class="row coucCard chooseConvert" key="${i}" 
          id="${myConverts.id}" name="${myConverts.name}"
          created_at="${myConverts.created_at}" growths="${growths}">
          <div class="roundedImg">
            <img src="img/assets/cal-l.jpg" />
          </div>
          <div class="coucText">
            <h4 style='text-transform : capitalize;'>${myConverts.name}</h4>
            <p>Growth Avg : ${growths}%</p>
          </div>
        </div>`;
      $(".listOfConverts").append(myConvertsRow);
    });
    loaderHide()
    let chosesnConvert = document.querySelectorAll(".chooseConvert");
    for (let d = 0; d < chosesnConvert.length; d++) {
      chosesnConvert[d].addEventListener(
        "click", function () {
          let convertsId = this.id
          $('.selectedConvertsName').text($(this).attr('name'))
          let convertCard = `
          <div class="convertsDetailImg">
            <img class="selectedConvertsImg" src="img/assets/Thumbnail-1.png" />
          </div>
          <div class="convertsDetailText">
            <h4>${$(this).attr('name')}</h4>
            <p> Growth Avg: ${$(this).attr('growths')}%</p>
            <p> Since: ${$(this).attr('created_at')} </p>
          </div>`
          $('.convertsDetail').append(convertCard)
          $(".pages").hide();
          $(".selectConvertsOptions").show();
          let councOptions = document.querySelectorAll('.councOptions')
          for (let d = 0; d < councOptions.length; d++) {
            councOptions[d].addEventListener(
              "click",
              function () {
                switch (this.id) {
                  case 'Analytic':
                    growthAnalytics(true, convertsId)
                    break
                  case 'Chat':
                    ChatView(this.id, convertsId)
                    break
                  case 'Profile':
                    break
                  case 'Ping':
                    break
                }
              },
              true
            );
          }
        }, true
      );
    }
  });
  $(".navPlaceOW").click(() => {
    $(".pages").hide();
    $(".placeOfWorship").show();
  });
  $(".sendPoWRequest").click(() => {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    let token = userProfile.token;

    if ($("#worshipCity").val() === "" || $("#worshipState").val() === "" || $("#worshipCountry").val() === "" || $("#worshipNote").val() === "") {
      $(".powErr").text("Please fill in all fields");
      $(".powErr").show();
    }
    else {
      $(".powErr").hide();
      $.ajax({
        url: sourceUrl + "save_worship_places",
        type: "POST",
        crossDomain: true,
        headers: {
          Authorization: "Bearer " + token,
        },
        data: {
          user_id: userProfile.user.id,
          city: $("#worshipCity").val(),
          state: $("#worshipState").val(),
          country: $("#worshipCountry").val(),
          request: $("#worshipNote").val(),
        },
        dataType: "json",
        error: () => {
          loaderHide()
        },
        success: (result) => {
          if (result.status == true) {
            loaderHide()
            $("#modalMessage").text("Your request has been sent successfully!");
            $("#modalConfirmation").modal("show");
          }
        },
      })
    }
  });
  $('.addConvBtn').click(() => {
    $(".pages").hide();
    $(".addConvert").show();
  })
  $(".navCouncel").click(() => {
    $(".pages").hide();
    $(".counselPage").show();
    // if (isConvert()) {
    //   $(".cvFooterDown").show()
    // }
    // else {
    //   $(".swvFooterDown").show()
    // }
    let chosesnSection = document.querySelectorAll(".councOptions");
    for (let d = 0; d < chosesnSection.length; d++) {
      chosesnSection[d].addEventListener(
        "click",
        function () {
          chooseCounselorView(this.id)
        },
        true
      );
    }
  });
  $(".cvNavGrowthAnaly").click(() => {
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    growthAnalytics(false, userProfile.user.id)
  });
  $(".notification").click(() => {
    $(".pages").hide();
    $(".notificationPage").show();
  });
  function storeActivity(activity_id, user_activity_id, response){
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    $.ajax({
      url: `${sourceUrl}saveActivity`,
      type: 'POST',
      data: {
        activity_id: activity_id,
        user_activity_id: user_activity_id,
        user_id: userProfile.user.id,
        answer: response
      },
      crossDomain: true,
      headers: { Authorization: "Bearer " + userProfile.token },
      dataType: "json",
      error: (err) => {
        loaderHide();
        if (err.status == 400) {
          alertFunction('Sorry, you can`t edit the activity responce')
        }
      },
      success: result => {
        taskAndActivities()
        loaderHide()
      }
    })
  }
  function taskAndActivities () {
    loaderShow()
    let userProfile = JSON.parse(localStorage.getItem("LoginResult"));
    $.ajax({
      url: sourceUrl + `activities/${userProfile.user.id}`,
      type: "GET",
      crossDomain: true,
      headers: { Authorization: "Bearer " + userProfile.token },
      dataType: "json",
      error: () => loaderHide(),
      success: (result) => {
        $(".activitiesList").text("");
        result.activities.map(activity => {
          let activitiesList = `
            <div class="vCenter row taskCard taskCategories" id="${activity.id}" name="${activity.type}" key="${activity.id}" question="${activity.question}">
              <div class="vCentered col-xs-3">
                <p style="text-transform: capitalize;"> <span class="zmdi zmdi-calendar zmdi-hc-2x" style="padding-right: 5px"></span> ${activity.frequency_description} </p>
              </div>
              <div class="row col-xs-8">
                <p> ${activity.type} </p>
                <p> <span></span> ${activity.message} </p>
              </div>
              <div class="vCentered col-xs-1">
                <p><span class="fa fa-chevron-right"></span></p>
              </div>
            </div>`
          $(".activitiesList").append(activitiesList);
        })
        loaderHide()
        $(".pages").hide();
        $(".convertTasks").show();

        let eachTasks = document.querySelectorAll(".taskCategories");
        $(".tasksList").text("");
        for (let d = 0; d < eachTasks.length; d++) {
          eachTasks[d].addEventListener(
            "click", function () {
              $('#taskCatName').text($(this).attr('name'))
              $('#taskQuestion').text($(this).attr('question'))
              let activity_id = this.id
              result.userActivities[activity_id].map(task => {
                //Populate User Activity
                let taskList = `
                  <div class="row taskCard taskBtn" id="${task.id}">
                    <div class="col-xs-2">
                      <p>
                        <span class="zmdi zmdi-calendar-alt zmdi-hc-2x" style="padding-right: 5px"></span>
                      </p>
                    </div>
                    <div class="col-xs-8"> <p class="taskDay">${task.created_at}</p> </div>
                    <div class="col-xs-2"> <p><span class="zmdi ${(task.done == '1') ? " zmdi-check-circle " : " zmdi-minus-circle-outline "} zmdi-hc-2x"></span></p> </div>
                  </div>`
                $(".tasksList").append(taskList);
                //Take an activity
                let eachTasks = document.querySelectorAll(".taskBtn");
                for (let d = 0; d < eachTasks.length; d++) {
                  eachTasks[d].addEventListener(
                    "click", function () {
                      $("#modalDidYou").modal("show")
                      $(".taskYes").click(() => {
                        storeActivity(activity_id, this.id, '1')
                        $("#modalDidYou").modal("hide")
                      })
                      $(".taskNo").click(() => {
                        storeActivity(activity_id, this.id, '0')
                        $("#modalDidYou").modal("hide")
                      })
                    }
                  )
                }
                $(".pages").hide();
                $(".tasksView").show();
              })
            }, false
          )
        }

      }
    })
  }
  $(".taskAndActivities").click(() => {
    taskAndActivities()
  });

  $(".navConvertsDev").click(() => {
    $(".pages").hide();
    $(".convertsDevelopment").show();
  })
  $(".navResources").click(() => {
    $(".pages").hide();
    $(".resources").show();
  })
  $(".bibleDoctrineBtn").click(() => {
    topicsView('5', 'Bible Doctrines')
  })
  $(".breakingAddictionBtn").click(() => {
    topicsView('8', 'Breaking Addiction')
  })
  $(".studyGuideBtn").click(() => {
    topicsView('4', 'Study Guide')
  })
  $(".discipleProgTab").click(() => {
    topicsView('1', 'Discipleship Program')
  })
  $(".convertStudyTab").click(() => {
    topicsView('2', 'Converts Studies Series')
  })
  //Soul Winner Digest
  $(".soulWinnerTab").click(() => {
    $(".pages").hide();
    $(".soulWinnerDigestSubs").show();
  })
  $(".evangRoadTab").click(() => {
    topicsView('3', 'Evangelism Roadmap')
  })
  $(".evangTechTab").click(() => {
    topicsView('6', 'Evangelism Technique')
  })
  $(".soulWinnerInYouTab").click(() => {
    topicsView('7', 'Soulwinners in You')
  })
  $(".chatsBtn").click(() => {
    $(".pages").hide();
    $(".chats").show();
  })
  $(".swvStudy").click(() => {
    $(".pages").hide();
    $(".swvAllResources").show();
  })
})