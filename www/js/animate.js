$(document).ready(function(){
  $("#demoBtn").on("hide.bs.collapse", function(){
    $(".collapseBtn").html('<span class="fa fa-angle-down fa-lg"></span>');
  });
  $("#demoBtn").on("show.bs.collapse", function(){
    $(".collapseBtn").html('<span class="fa fa-angle-up fa-lg"></span>');
  });
  $("#drops").on("hide.bs.collapse", function(){
    $(".collapseBtn").html('<span class="fa fa-angle-down fa-lg"></span>');
  });
  $("#drops").on("show.bs.collapse", function(){
    $(".collapseBtn").html('<span class="fa fa-angle-up fa-lg"></span>');
  });
}); 